@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-lg-6 col-lg-offset-3">
        <form action="/create/todo" method="POST">
            {{ csrf_field() }}
            <input type="text" name="todo" class="form-control input-lg" placeholder="Create todo">
        </form>
    </div>
</div>
<hr>
@foreach ($todos as $todo)

{{ $todo->todo }} <a href="{{ route('todo.delete', ['id' => $todo->id]) }}" class="btn btn-danger float-right">x</a>
<a href="{{ route('todo.update', ['id' => $todo->id]) }}" class="btn btn-info float-right btn-sm mr-2">update</a>
@if (!$todo->completed)
<a href="{{ route('todo.completed', ['id' => $todo->id ]) }}" class="btn btn-success btn-sm mr-2 float-right ">Mark completed</a>
@else
<span class="text-success float-right mr-2">Completed!</span>
@endif
    <hr>
@endforeach
    
@endsection